Framework Release Notes and Changes
==========================
*v2.3*
------
* **CHANGES MAVEN REPOSITORY**
* **CHANGES ARTIFACT AND GROUP**
``gradle

repositories {
    maven{ url = "http://variable-android.s3-website-us-east-1.amazonaws.com/release" }
}

dependencies {
    compile 'com.variable:node-android-framework:2.3'
}
``
* All new chroma api interface.

* ConnectionListener has been slightly altered to incorporate multiple color sensor class (i.e. photon and chroma).

* ChromaDevice methods have been changed to implement ColorSensor interface. (i.e. all requestChromaReading are now requestColor)

* To use this portion of the api, you will need a access key and library access key provided by Variable. (Please contact us for more info)
