/* See http://variableinc.com/terms-use-license for the full license governing this code. */
package com.variable.demo.api.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.variable.demo.api.NodeApplication;
import com.variable.demo.api.R;
import com.variable.framework.node.ChromaDevice;
import com.variable.framework.node.NodeDevice;
import com.variable.framework.node.enums.NodeEnums;

import java.text.DecimalFormat;

/**
 * Created by Corey Mann on 8/28/13.
 */
public class ChromaScanFragment extends ChromaFragment {


    public static final String TAG = ChromaScanFragment.class.getName();
    private final DecimalFormat formatter = new DecimalFormat("###.##");

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup view, Bundle savedInstanced){
        super.onCreateView(inflater,view, savedInstanced);

        final View rootView = inflater.inflate(R.layout.single_scan, null, false);
        rootView.findViewById(R.id.btnSingleScan).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               mChroma.requestColor();
            }
        });

        rootView.findViewById(R.id.btnCalibrate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mChroma.getChromaModuleInfo().getModel().equals("1.1"))
                {
                    Toast.makeText(getActivity(), "Calibration not available for Chroma Older than 1.1", Toast.LENGTH_LONG).show();
                    return;
                }
                mChroma.requestWhitePointCal();
            }
        });
        return rootView;
    }

    @Override
    public void onColorUpdate(int color){
        super.onColorUpdate(color);

        getView().findViewById(R.id.imgScanColor).setBackgroundColor(color);
    }

    @Override
    public void onRGBUpdate(double r, double g, double b){
        super.onRGBUpdate(r, g, b);

        String text = formatter.format(r) + " , " + formatter.format(g) + " , " + formatter.format(b);
        ((TextView) getView().findViewById(R.id.txtRGB)).setText(text);
    }


    @Override
    public void onLABUpdate(double l, double a, double b){
        super.onLABUpdate(l, a, b);
        String text = formatter.format(l) + " , " + formatter.format(a) + " , " + formatter.format(b);
        ((TextView) getView().findViewById(R.id.txtLab)).setText(text);
    }


    @Override
    public void onHexValue(String hex){
        super.onHexValue(hex);
        ((TextView) getView().findViewById(R.id.txtHex)).setText(hex);
    }
}
