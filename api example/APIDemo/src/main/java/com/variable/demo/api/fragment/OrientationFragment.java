package com.variable.demo.api.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.variable.demo.api.NodeApplication;
import com.variable.demo.api.R;
import com.variable.framework.dispatcher.DefaultNotifier;
import com.variable.framework.node.MotionSensor;
import com.variable.framework.node.NodeDevice;
import com.variable.framework.node.reading.VTQuatReading;
import com.variable.framework.node.reading.VTYPRReading;

/**
 * Created by coreymann on 2/17/16.
 */
public class OrientationFragment extends Fragment implements MotionSensor.OrientationListener{
    public static final String TAG = OrientationFragment.class.getName();

    private TextView txtW, txtX, txtY, txtZ;
    private final Handler mHandler = new Handler(Looper.getMainLooper());

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View root = inflater.inflate(R.layout.orientation, container, false);
        txtY = (TextView) root.findViewById(R.id.txtY);
        txtW = (TextView) root.findViewById(R.id.txtW);
        txtZ = (TextView) root.findViewById(R.id.txtZ);
        txtX = (TextView) root.findViewById(R.id.txtX);

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();

        DefaultNotifier.instance().addAHRSListener(this);

        NodeDevice device = NodeApplication.getActiveNode();
        if(device != null && device.getMotionSensor() != null){
            device.getMotionSensor().setOrientationStreamMode(false, true);
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        DefaultNotifier.instance().removeAHRSListener(this);

        NodeDevice device = NodeApplication.getActiveNode();
        if(device != null && device.getMotionSensor() != null){
            device.getMotionSensor().setOrientationStreamMode(false, false);
        }
    }

    @Override
    public void onYPRReading(MotionSensor sensor, VTYPRReading reading) {
        //no longer implemented
    }

    @Override
    public void onAHRSReading(MotionSensor sensor, final VTQuatReading reading) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                txtX.setText(String.format("%.3f", reading.getQ0()));
                txtY.setText(String.format("%.3f", reading.getQ1()));
                txtZ.setText(String.format("%.3f", reading.getQ2()));
                txtW.setText(String.format("%.3f", reading.getQ3()));
            }
        });
    }
}
