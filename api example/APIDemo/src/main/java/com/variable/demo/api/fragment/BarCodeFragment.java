/* See http://variableinc.com/terms-use-license for the full license governing this code. */
package com.variable.demo.api.fragment;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.variable.demo.api.NodeApplication;
import com.variable.demo.api.R;
import com.variable.framework.dispatcher.DefaultNotifier;
import com.variable.framework.node.BarCodeScanner;
import com.variable.framework.node.enums.NodeEnums;
import com.variable.framework.node.reading.SensorReading;

/**
 *
 */
public class BarCodeFragment extends Fragment {
    public static final String TAG = BarCodeFragment.class.getName();

    private BarCodeScanner scanner;
    private BarCodeScanner.BarCodeScannerListener listener;
    public BarCodeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Obtain the Active Scanner.
        scanner = NodeApplication.getActiveNode().findSensor(NodeEnums.ModuleType.BARCODE);
        assert scanner != null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.barcode, null, false);

        root.findViewById(R.id.btnScan).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scanner.requestBarCodeScan();
            }
        });

        //Create the adapter to holdall the barcodes scanned
        final EditText editText = (EditText) root.findViewById(R.id.editBarCode);
        final ToggleButton barCodeBtnEnabled = (ToggleButton) root.findViewById(R.id.barcodeEnabeldToggleButton);
        barCodeBtnEnabled.setChecked(scanner.isButtonScanEnabled());
        barCodeBtnEnabled.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                scanner.requestBarcodeBtnEnabled(isChecked);
            }
        });
        listener = new BarCodeScanner.BarCodeScannerListener() {
            @Override
            public void onBarCodeTransmitted(BarCodeScanner barCodeScanner, final SensorReading<String> barCodeReading) {
                        editText.setText(barCodeReading.getValue());
            }

            @Override
            public void onBarcodeButtonSettingUpdated(final BarCodeScanner barCodeScanner) {
                        barCodeBtnEnabled.setChecked(barCodeScanner.isButtonScanEnabled());
            }
        };
        return root;
    }

    @Override
    public void onResume()
    {
        super.onResume();
        DefaultNotifier.instance().addBarCodeScannerListener(listener);
    }

    @Override
    public void onPause()
    {
        super.onPause();
        DefaultNotifier.instance().addBarCodeScannerListener(listener);
    }



}
