package com.variable.demo.api.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import com.variable.demo.api.NodeApplication;
import com.variable.demo.api.R;
import com.variable.framework.node.LumaSensor;
import com.variable.framework.node.NodeDevice;
import com.variable.framework.node.enums.NodeEnums;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

//TODO: Add Settings Page.

/**
 * Created by corey mann on 7/28/13.
 */
public class LumaFragment extends Fragment {
    public static final String TAG = LumaFragment.class.getName();

    private LumaSensor luma;
    private View mSelectedLumaModeView;

    private Timer mTimer = new Timer();

    private SeekBar mSeekBar;
    private TextView mSeekBarText;

    private TimerTask mActiveLumaTask;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View root = inflater.inflate(R.layout.luma, null, false);

        NodeDevice node = ((NodeApplication) getActivity().getApplication()).getActiveNode();
        luma = node.findSensor(NodeEnums.ModuleType.LUMA);

        root.findViewById(R.id.btnTurnOff).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onModeChanged(LumaMode.OFF);
            }
        });

        //Seek Bar Setup
        mSeekBar = (SeekBar) root.findViewById(R.id.seekBrightness);
        mSeekBar.setProgress(0);
        mSeekBar.setMax(9);
        mSeekBar.setOnSeekBarChangeListener(new SeekBarChangedListener());
        mSeekBar.setTag(Boolean.TRUE);
        mSeekBar.setEnabled(true);
        mSeekBarText = (TextView) root.findViewById(R.id.seekBarText);

        LumaModeOnClickListener onClickListener = new LumaModeOnClickListener();

        //Init Manual Button
        View view = root.findViewById(R.id.btnManual);
        view.setOnClickListener(onClickListener);
        view.setTag(LumaMode.Manual);

        //Check that there is an active node first.
        if (luma != null) {
            onClickListener.onClick(view); //Default to Manual Mode
        }

        //Init Flash Button
        view = root.findViewById(R.id.btnFlash);
        view.setOnClickListener(onClickListener);
        view.setTag(LumaMode.Flash);

        view = root.findViewById(R.id.btnHiSpeed);
        view.setOnClickListener(onClickListener);
        view.setTag(LumaMode.HiSpeed);

        //Init SOS Button
        view = root.findViewById(R.id.btnSOS);
        view.setOnClickListener(onClickListener);
        view.setTag(LumaMode.SOS);

        return root;
    }

    /**
     * onEnterManualMode occurs when LUMA's brightness is user controlled.
     */
    protected void onEnterManualMode() {
        if (mActiveLumaTask != null) {
            mActiveLumaTask.cancel();
            mTimer.purge();
            mTimer = new Timer();
        }

        //Update the TAG for the OnSeekChanged Listener
        mSeekBar.setTag(Boolean.TRUE);
        mSeekBar.setEnabled(true);

        mSeekBar.setMax(255);
        mSeekBar.setProgress(1);
        mSeekBarText.setText("Brightness");


        int progress;
        byte b;

        if (getView() != null) {
            progress = ((SeekBar) getView().findViewById(R.id.seekBrightness)).getProgress();
            b = normalizeBrightness(progress);
        } else {
            b = 1;
        }

        onBrightnessChanged(b);
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        luma.setLumaMode((short)0x00);
    }

    /**
     * By scheduling a reoccuring SOS task, luma becomes a SOS beacon.
     */
    protected void onEnteredSOSMode() {
        if (mActiveLumaTask != null) {
            mActiveLumaTask.cancel();
            mTimer.purge();
            mTimer = new Timer();
        }


        mActiveLumaTask = new LumaSOSTask();
        mTimer.scheduleAtFixedRate(mActiveLumaTask, 0, 7000);

        mSeekBar.setEnabled(false);
    }

    /**
     * Invoked when LUMA is entering Flash mode.
     * Flash mode is a consistent re-occuring cycle. During this cycle
     * LUMA is sent byte 0 - 255 depending on the speed selection from the seekbar.
     */
    protected void onEnteredFlashMode() {

        if (mActiveLumaTask != null) {
            mActiveLumaTask.cancel();
            mTimer.purge();
            mTimer = new Timer();
        }
        mActiveLumaTask = new LumaFlashTimerTask();
        mTimer.scheduleAtFixedRate(mActiveLumaTask, 0, 4000);

        //Update the TAG for the OnSeekChanged Listener
        mSeekBar.setTag(Boolean.TRUE);

        mSeekBar.setMax(10);
        mSeekBar.setProgress(500);
        mSeekBarText.setText("Speed");

    }

    /**
     * Invoked when the Mode has changed and only changed.
     *
     * @param mode
     */
    private void onModeChanged(LumaMode mode) {
        if (mode == LumaMode.Manual) {
            onEnterManualMode();

        } else if (mode == LumaMode.Flash) {
            onEnteredFlashMode();

        } else if (mode == LumaMode.SOS) {
            onEnteredSOSMode();

        } else if (mode == LumaMode.OFF) {
            mActiveLumaTask.cancel();

            //Turn off Luma
            luma.setLumaMode((byte) 0);

        } else if (mode == LumaMode.HiSpeed) {
            onEnteredHiSpeedMode();
        }

    }

    protected void onEnteredHiSpeedMode() {
        if (mActiveLumaTask != null) {
            mActiveLumaTask.cancel();
            mTimer.purge();
            mTimer = new Timer();
        }

        mActiveLumaTask = new LumaHiSpeedTimerTask();
        mTimer.schedule(mActiveLumaTask, new Date());

        mSeekBar.setEnabled(false);

    }

    /**
     * Handles changes in the brightness of luma.
     *
     * @param brightnesslevel - number of LED's to turn on
     */
    protected void onBrightnessChanged(byte brightnesslevel) {
        luma.setLumaMode(brightnesslevel);
    }

    /**
     * Handles changes in the speed in which luma is sent commands.
     *
     * @param speed in seconds.
     */
    protected void onSpeedChanged(int speed) {
        speed = speed == 0 ? 1000 : speed * 1000;

        if (mActiveLumaTask != null) {
            mActiveLumaTask.cancel();
            mTimer.purge();
            mTimer = new Timer();
        }
        mActiveLumaTask = new LumaFlashTimerTask().setRate(speed);
        mTimer.scheduleAtFixedRate(mActiveLumaTask, 0, speed * 256); //255 Cycles * Sleep  Time of Thread
    }

    private class LumaModeOnClickListener implements View.OnClickListener {
        private final int pressedColor;
        private final int defaultColor;

        public LumaModeOnClickListener() {
            pressedColor = getResources().getColor(R.color.dark_blue);
            defaultColor = getResources().getColor(R.color.dark_gray);
        }


        @Override
        public void onClick(View view) {
            if (mSelectedLumaModeView != view) {

                if (mSelectedLumaModeView != null) {
                    //Reset to default button color
                    mSelectedLumaModeView.setBackgroundColor(defaultColor);
                }

                //Set as the selected mode
                mSelectedLumaModeView = view;

                //Change the color to the active blue.
                mSelectedLumaModeView.setBackgroundColor(pressedColor);

                //fire event
                onModeChanged((LumaMode) view.getTag());
            }


        }
    }

    private class LumaFlashTimerTask extends TimerTask {
        private AtomicBoolean mIsAlive = new AtomicBoolean(true);
        private AtomicInteger mRate = new AtomicInteger(100);

        @Override
        public boolean cancel() {
            mIsAlive.set(false);
            return super.cancel();
        }


        public LumaFlashTimerTask setRate(int rate) {
            mRate.set(rate);
            return this;
        }

        @Override
        public void run() {
            for (short i = 0; i < 256; i++) {
                if (mIsAlive.get()) {

                    luma.setLumaMode(normalizeBrightness(i));

                    try {
                        Thread.sleep(mRate.get());
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    //Kill Task.
                } else {
                    break;
                }
            }
        }
    }

    private class LumaHiSpeedTimerTask extends TimerTask {
        private AtomicBoolean mIsAlive = new AtomicBoolean(true);

        @Override
        public boolean cancel() {
            mIsAlive.set(false);
            return super.cancel();
        }

        @Override
        public void run() {
            while (true) {
                for (short i = 0; i < 8; i++) {
                    if (!mIsAlive.get()) { break; }
                    luma.setLumaMode((short) (0x01 << (i % 8)));
                    try {   Thread.sleep(1000/60);  } catch (InterruptedException e) {  }
                }
            }
        }

    }

    /**
     * This task performs a single SOS scan.
     */
    private class LumaSOSTask extends TimerTask {
        private final AtomicBoolean mIsAlive = new AtomicBoolean(true);

        @Override
        public boolean cancel() {
            mIsAlive.set(false);
            return super.cancel();
        }


        @Override
        public void run() {
            for (int i = 0; i < 3; i++) {
                if (!mIsAlive.get()) {
                    return;
                }

                showLight(500);
            }

            waitCycles(500);

            for (int i = 0; i < 3; i++) {
                if (!mIsAlive.get()) {
                    return;
                }

                showLight(1500);
            }

            waitCycles(500);

            for (int i = 0; i < 3; i++) {
                if (!mIsAlive.get()) {
                    return;
                }

                showLight(500);
            }
        }

        public void showLight(int lengthInMillis) {
            luma.setLumaMode((short) 255);

            waitCycles(lengthInMillis);

            luma.setLumaMode((byte) 0);

            waitCycles((int) (lengthInMillis * .25));
        }

        public void waitCycles(int millis) {
            try {
                Thread.sleep(millis);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    private byte normalizeBrightness(int progressFromBar) {
        short progress = (short) progressFromBar;
        int ledsOn = (progress << 1) | (progress >> 7);

        return (byte) ledsOn;
    }

    private class SeekBarChangedListener implements SeekBar.OnSeekBarChangeListener {


        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean isFromUser) {
            Log.d(TAG, "onProgressChanged - " + progress);
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            Log.d(TAG, "onStartTrackingTouch - ");
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            Log.d(TAG, " onStopTrackingTouch - " + seekBar.getProgress());
            int progress = seekBar.getProgress();

            if (((Boolean) seekBar.getTag())) {
                onBrightnessChanged(normalizeBrightness(progress));
            } else {
                onSpeedChanged(progress);
            }

        }
    }

    /**
     * Lists the type of modes LUMA supports.
     */
    public enum LumaMode {
        SOS, Manual, Flash, HiSpeed, OFF
    }
}
