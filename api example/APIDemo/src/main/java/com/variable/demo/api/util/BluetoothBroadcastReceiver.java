package com.variable.demo.api.util;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.variable.framework.BuildConfig;
import com.variable.framework.devices.VTDevice;
import com.variable.framework.dispatcher.DefaultNotifier;
import com.variable.framework.manager.NodeManager;
import com.variable.framework.node.ColorMuseDevice;
import com.variable.framework.node.NodeDevice;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static com.variable.framework.node.NodeDevice.BluetoothDeviceTypes.COLOR_MUSE_LOW_ENERGY;


/**
 * Bluetooth BroadcastReceiver, handles discovery requests, incoming pair requests, and discovery start/finished actions. 
 *
 *  <BR />Specifically it registers the following actions,
 *  {@link android.bluetooth.BluetoothDevice#ACTION_FOUND},<BR />
 *  {@link android.bluetooth.BluetoothAdapter#ACTION_DISCOVERY_FINISHED}, <BR />
 *  {@link android.bluetooth.BluetoothAdapter#ACTION_DISCOVERY_STARTED}, <BR />
 *  {@link android.bluetooth.BluetoothDevice#ACTION_BOND_STATE_CHANGED}
 *
 * Created by Corey Mann on 14/11/2014.
 *
 */
public class BluetoothBroadcastReceiver extends BroadcastReceiver {

    /**
     * Constructs a new receiver for the context.
     * @param context - the activity context.
     * @param discoveryTimeout - the discovery timeout in ms. It is suggested if you are looking for LE devices to make this less than 8000
     */
    public BluetoothBroadcastReceiver(Context context, int discoveryTimeout){
        this(context);
        this.mDiscoveryTimeout = discoveryTimeout;
    }
    /**
     * Constructs a new receiver for this context.  
     * @param context - the activity context.
     */
    public BluetoothBroadcastReceiver(Context context){ 
        super();
        this.mDiscoveryTimeout = 8000;
        this.mContext = context;
        this.mHandler = new Handler(context.getMainLooper());
        this.mNodeDeviceFactory = new NodeDevice.Factory();
    }

    /**
     * Sets the factory instance for creating node device objects.
     *
     * By default this
     * @param factory
     */
    public BluetoothBroadcastReceiver(Context context, NodeDevice.Factory factory){
        this.mDiscoveryTimeout = 8000;
        this.mContext = context;
        this.mHandler = new Handler(context.getMainLooper());
        this.mNodeDeviceFactory = factory;
    }

    private final NodeDevice.Factory mNodeDeviceFactory;
    private boolean registeredEvents, performLEScan;
    private int mDiscoveryTimeout;
    private final Handler mHandler;
    private final Context mContext;
    private DiscoveryCompletedListener onDiscoveryFinishedListener;
    private DiscoveryStartedListener onDiscoveryStartedListener;
    private int pairingAttempts = 0;
    private boolean mEnableAutoConnect = true;

    public void enableAutoConnectionAfterPairing(boolean enable){
        this.mEnableAutoConnect = enable;
    }
    /**
     * Performs a binding to the Chroma Service
     * Registers the {@link com.variable.framework.android.bluetooth.BluetoothBroadcastReceiver}
     */
    public void register()
    {
        registeredEvents = true;

        //Register for Bluetooth Events.
        IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        filter.addAction(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
        this.mContext.registerReceiver(this, filter);
    }

    /**
     * Unregisters the context used in this BroadcastReceiver.
     */
    public void unregister(){
        registeredEvents = false;
        this.mContext.unregisterReceiver(this);
    }
    
    public void setOnDiscoveryCompletedListener(DiscoveryCompletedListener l){
        this.onDiscoveryFinishedListener = l;
    }
    public void setOnDiscoveryStartedListener(DiscoveryStartedListener l){
        this.onDiscoveryStartedListener = l;
    }

    @TargetApi(18)
    @Override
    public void onReceive(Context context, @NonNull Intent intent) {
        String action = intent.getAction();
        if(BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)){
            if(onDiscoveryFinishedListener != null){ onDiscoveryFinishedListener.onDiscoveryCompleted();}

        }else if(BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)){
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if(registeredEvents) {
                        Log.w(BuildConfig.TAG, "canceling discovery");
                        BluetoothAdapter.getDefaultAdapter().cancelDiscovery();
                    }
                }
            }, mDiscoveryTimeout);
            
            if(onDiscoveryStartedListener != null) { onDiscoveryStartedListener.onDiscoveryStarted();}

        }else if(BluetoothDevice.ACTION_FOUND.equals(action)) {
            NodeDevice node = mNodeDeviceFactory.fromDiscoveryIntent(context.getApplicationContext(), intent);
            if(node != null){
                DefaultNotifier.instance().dispatchOnDiscovery(node);
            }

            //Bond Attempt was successful
        } else if(BluetoothDevice.ACTION_BOND_STATE_CHANGED.equals(action)) {
            if(mEnableAutoConnect) {

                //Parse the Previous State out
                int previousBondedState = intent.getIntExtra(BluetoothDevice.EXTRA_PREVIOUS_BOND_STATE, -1);

                //Obtain the Node instance.
                NodeDevice node = mNodeDeviceFactory.fromDiscoveryIntent(context.getApplicationContext(), intent);

                //Override the original extra with what is reflected on the BluetoothDevice. this seems to be more accurate.
                int newBondState = ((BluetoothDevice) intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE)).getBondState();

                Log.d("BondState", String.format("%d -> %d", previousBondedState, newBondState));
                if (node != null) {
                    onBondStateChanged(node, newBondState, previousBondedState);
                }
            }
        }
    }

    private void onBondStateChanged( NodeDevice node, int newBondState, int previousBondedState) {
        if(node == null) { Log.d(BuildConfig.TAG, "Ignoring - NODE Not Present"); return; }

        //The Bluetooth Device is still not bonded.
        if (newBondState == BluetoothDevice.BOND_NONE && previousBondedState == BluetoothDevice.BOND_BONDING) {

            //Increment and Check if we have exceeded the amount of retries.
            if (++pairingAttempts > 1) {

                //Reset the Pairing Attempts...
                pairingAttempts = 0;

                //Dispatch Pairing Failed...
                DefaultNotifier.instance().dispatchPairingFailure(node);
            } else {

                //Attempt a Retry...
                Log.d(BuildConfig.TAG, "Attempting Pairing Retry");
                node.connect();
            }
        } else if (newBondState == BluetoothDevice.BOND_BONDING) {
            Log.d(BuildConfig.TAG, "Bonding with " + node.getName());
        } else if (newBondState == BluetoothDevice.BOND_BONDED) {
            Log.d(BuildConfig.TAG, "Bond Successful with " + node.getName());
            node.connect();
        }
    }

    public interface DiscoveryStartedListener{
        void onDiscoveryStarted();
    }

    public interface DiscoveryCompletedListener{
        void onDiscoveryCompleted();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    public void startLowEnergyScan(String uuid){
        final BluetoothManager bluetoothManager = (BluetoothManager) this.mContext.getSystemService(Context.BLUETOOTH_SERVICE);
        bluetoothManager.getAdapter().cancelDiscovery();

        final BluetoothAdapter.LeScanCallback callback = new BluetoothAdapter.LeScanCallback() {
            @Override
            public void onLeScan(BluetoothDevice bluetoothDevice, int rssi, byte[] scanRecord) {
                NodeDevice node  = mNodeDeviceFactory.create(mContext, bluetoothDevice, COLOR_MUSE_LOW_ENERGY);
                DefaultNotifier.instance().dispatchOnDiscovery(node);
            }
        };
        bluetoothManager.getAdapter().startLeScan(new UUID[]{UUID.fromString(uuid) }, callback);

        DefaultNotifier.instance().postDelayed(new Runnable() {
            @Override
            public void run() {
                bluetoothManager.getAdapter().stopLeScan(callback);
            }
        }, TimeUnit.SECONDS.toMillis(8));
    }
}
