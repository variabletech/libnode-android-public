/* See http://variableinc.com/terms-use-license for the full license governing this code. */
package com.variable.demo.api.fragment;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.Toast;

import com.variable.demo.api.NodeApplication;
import com.variable.framework.chroma.service.datamodel.ChromaModuleInfo;
import com.variable.framework.devices.ColorSensor;
import com.variable.framework.node.reading.ColorConverter;
import com.variable.framework.node.reading.ColorSense;
import com.variable.framework.node.ChromaDevice;
import com.variable.framework.dispatcher.DefaultNotifier;
import com.variable.framework.node.NodeDevice;

import com.variable.framework.node.enums.NodeEnums;
import com.variable.framework.node.reading.SpectralColor;
import com.variable.framework.node.reading.VTRGBCReading;


import java.util.Date;


/**
 * ChromaFragment is a base fragment with no implementation specified for layouts and work flow.
 * Any instance will listen for new chroma scans and process them accordingly by passing the necessary values to its handler.
 *
 * Additionally, this will allow for requesting a chroma reading via a button pressed by default. If this behavior is not to be expected then
 * 
 * Created by coreymann on 6/28/13.
 */

public class ChromaFragment extends Fragment implements ChromaDevice.ChromaListener{
        public static final String TAG = ChromaFragment.class.getName();

        protected ChromaDevice mChroma;
        private NodeDevice.ButtonListener mButtonListener = new NodeDevice.ButtonListener() {
            @Override
            public void onPushed(NodeDevice nodeDevice) {
                Log.d(TAG, "onPushed()");
            }

            @Override
            public void onReleased(NodeDevice nodeDevice) {
                Log.d(TAG, "onReleased()");

                if(allowScanWhenButtonReleased()) {
                    //By sleeping, this will allow the downward pressure to be released and
                    // avoid the propagation of errors during a chroma scan.
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    //Issue a request for a new reading.
                    mChroma.requestColor();
                }
            }
        };


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            //Store the active chroma device.
            mChroma = NodeApplication.getActiveNode().findSensor(NodeEnums.ModuleType.CHROMA);
        }

        catch(ClassCastException ex) {
            Toast.makeText(getActivity(), "Normal Chroma usage requires an API Key", Toast.LENGTH_SHORT).show();
            getFragmentManager().beginTransaction().remove(this).commit();
        }
    }

    @Override
    public void onResume(){
        super.onResume();

        //Register for Chroma Scans.
        DefaultNotifier.instance().addButtonListener(mButtonListener);
        DefaultNotifier.instance().addChromaListener(this);
    }

    @Override
    public void onPause(){
        super.onPause();

        //UnRegister for Chroma Scans
        DefaultNotifier.instance().removeButtonListener(mButtonListener);
        DefaultNotifier.instance().removeChromaListener(this);
    }

    @Override
    public void onSpectrumReceived(@NonNull ColorSensor colorSensor, @NonNull SpectralColor spectralColor) {
         //Not Implementing in this demo
    }

    /**
     * invokes
     *
     *    onRGBUpdate
     *    onTimeStampUpdate
     *    onColorUpdate
     *    onLABUpdate
     *
     *
     *
     * @param moduleInfo
     * @param reading
     */
    @Override
    public void  onChromaReadingReceived(@NonNull ChromaModuleInfo moduleInfo, @NonNull VTRGBCReading reading) {
        ColorSense sense = reading.getColorSense();
    
        /** This demonstrates how to get color objects in D65 space
         *  If D50 is the space the requirement then pass Illuminate.D50 instead.
         * **/
        ColorConverter.XYZ d65XYZ = reading.getXYZ(ColorConverter.Illuminate.D65);
        
        //Performs on the fly calculation from D65 XYZ space to D65 Lab space.
        ColorConverter.Lab d65LAB = d65XYZ.toLab();
        
        Log.d(TAG, "SENSE_VALUES: " + sense.getSenseRed().floatValue() + " , " + sense.getSenseGreen() + " , " + sense.getSenseBlue() + " , " + sense.getSenseClear());
        
        //Performs on the fly calculation from D65 XYZ to sRGB.
        ColorConverter.RGB rgb = d65XYZ.toRGB(ColorConverter.RGBSpace.sRGB);

        //Performs on the fly calculation from D65 LAB to LCH.
        //ColorConverter.LCH lch = d65LAB.toLCH();
    
        onLABUpdate(d65LAB.getL(), d65LAB.getA(), d65LAB.getB());
        onHexValue(rgb.toHexString());
        onRGBUpdate(rgb.getRawRed(), rgb.getRawGreen(), rgb.getRawBlue());
        onColorUpdate(rgb.getColor());

    }

    /**
     * Invoked when a new reading has been received. Additionally, this method is invoked on the UI Thread.
     * @param red
     * @param green
     * @param blue
     */
    public void onRGBUpdate(double red, double green, double blue){ }

    /**
     * Invoked when a new reading has been received. Additionally, this method is invoked on the UI Thread.
     *
     * @param hexString - formatted such that #RRGGBB
     */
    public void onHexValue(String hexString){  }

    /**
     * Invoked when a new reading has been received. Additionally, this method is invoked on the UI Thread.
     * @param l
     * @param a
     * @param b
     */
    public void onLABUpdate(double l, double a, double b){ }

    /**
     * Invoked when a new reading has bee received. Additionally, this method is invoked on the UI Thread.
     * @param color
     */
    public void onColorUpdate(int color ) {  }



    public boolean allowScanWhenButtonReleased(){
        return true;
    }

    /**
     *
     *
     * @param device
     * @param temperature
     */
    @Override
    public void onChromaTemperatureReading(@NonNull ColorSensor device, @NonNull Float temperature) {
    }

    @Override
    public void onWhitePointCalComplete(ColorSensor device, boolean status) {
        Toast.makeText(getActivity(), "Calibration " + (status ? "succeeded" : "failed"), Toast.LENGTH_LONG).show();}
}
